particle check template
===========================

This project is intended as a template for doing automated tests of code to be used with [particle.io](https://particle.io) boron boards.

We use [cppcheck](https://sourceforge.net/projects/cppcheck/) for static check of code. It can check for at lot of things, see their [docs](https://sourceforge.net/p/cppcheck/wiki/ListOfChecks/)

Particle.io uses it internally also, see [here](https://docs.platformio.org/en/latest/plus/check-tools/cppcheck.html).

Use `check_ino.sh` to check .ino files and `check.cpp` to check cpp files.

We rely on the particle-cli module. Go [here](https://docs.particle.io/reference/developer-tools/cli/) for more info. The preprocess subcommand is used to convert .ino files to .cpp files (see `particle preprocess --help` for details)
